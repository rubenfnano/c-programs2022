
#include <stdio_ext.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) { 
    int n;
    char hex_val[50];
 
    n = atoi("678");
    sprintf(hex_val, "%x", n);
 
    printf("%s \n", hex_val);
    return EXIT_SUCCESS;
}

#include <stdio_ext.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX 30

int main () {

   char palabra[MAX];

   printf ("Escribe una palabra: \n");

   fgets (palabra, MAX, stdin);

   int last = strlen (palabra);
   palabra[last-1]  = '\0';

   printf ("Tu palabra %s\n", palabra);

    return EXIT_SUCCESS;
}
